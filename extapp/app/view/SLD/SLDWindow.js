Ext.define('SLDstyler.view.SLD.SLDWindow',{
    extend: 'Ext.window.Window',
    requires: [
        'SLDstyler.view.SLD.SLDWindowController'
    ],
    alias: 'widget.sldwindow',
    controller: 'sldcontroller',
    width: 500,
    height: 500,
    title: 'SLD creation',

    items: [
    {
        xtype: 'form',
        items: [
            {
        xtype: 'fieldset',
        title: 'Create SLD',
        defaultType: 'textfield',

        items: [
                    {
                xtype: 'colorpicker',
                value: '993300',
                listeners: {
        select: 'alertSelectedColor'
    }
            },
                    {
        xtype: 'textfield',
        name: 'textfield1',
        fieldLabel: 'Text field',
        value: 'Text field value'
    },
            {
                xtype: 'button',
                text: 'click me please',
                handler: 'alertUser'
            }
        ]
    }
        ]
    }
    ]
});