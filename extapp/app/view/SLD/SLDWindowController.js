Ext.define('SLDstyler.view.SLD.SLDWindowController',{

    extend: 'Ext.app.ViewController',
    alias: 'controller.sldcontroller',

    init: function(){
        console.log('sld controller initialized')
    },

    alertUser: function(){
        alert('Hello there, add functionality to this part of the application!')
    },

    alertSelectedColor: function(picker, selectedColor){
        alert('Color code is: '.concat(selectedColor))
    }
});