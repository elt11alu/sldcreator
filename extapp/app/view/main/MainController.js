/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SLDstyler.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    requires:[
           'SLDstyler.view.SLD.SLDWindow'
    ],

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    openSLDWindow: function(){
        var win = Ext.create({
            xtype: 'sldwindow'
        });

        win.show()
    }
});
